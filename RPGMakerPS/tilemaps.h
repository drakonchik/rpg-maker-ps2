#ifndef TILEMAPS_H
#define TILEMAPS_H

#include <string>

#include <QGraphicsScene>

typedef struct
{

}PSTileMap;

typedef struct
{

}NPC;

typedef struct
{

}Shop;

typedef struct
{

}Door;

typedef struct
{

}Chest;

class TileMaps
{


public:
    TileMaps();
    void AddTileMap(std::string MapName, int Width, int Height, int Tileset, std::string Directory);
    void OpenTileMap(std::string MapName, int Tileset, std::string Directory);
    int GetTileMapArray(int i, int j);
    void SetUpTileset();
    void SetTileMap(std::string MapName, std::string Directory);
    void ReadTileMapHeader(std::string text);
    int* ReadTileMapLayer(std::string text);
    void ReadTileMapEvents();
    void SetTileMapArray(int i, int j);

    std::string TileMapName;
    int TilesetID;
    int MapWidth;
    int MapHeight;
    int TileMapArraySize;

    int* TileMapArray1;
    int* TileMapArray2;
    int* TileMapArray3;
};

#endif // TILEMAPS_H
