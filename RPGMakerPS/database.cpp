/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : RPG BINARY FILE DATA HANDLER AND EDITOR                  */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : database.cpp                                             */
/*  User Interface: database.ui                                              */
/*                                                                           */
/*****************************************************************************/

/* #### Language Header Files #### */
#include<iostream>
#include<fstream>

/* #### QLibrary Header Files #### */
#include <QMessageBox>

/* #### User Header Files     #### */
#include "database.h"
#include "ui_database.h"
#include "stringblob.h"

/* #### User Header Struct Arrays  #### */
Ui::Actor actors[256];
Ui::Item items[256];
Ui::Spell spells[256];
Ui::Weapon weapons[256];
Ui::Armour armours[256];
Ui::Enemy enemies[256];
Ui::Classy classy[256];

//std::string textData[4096];

//int textDataLineCount = 0;

/* #### Array and QList memory variables #### */
int previousListActors = -1;
int previousListClasses = -1;
int previousListSpells = -1;
int previousListItems = -1;
int previousListWeapons = -1;
int previousListArmours = -1;
int previousListEnemies = -1;

/* #### User Defined Variables #### */
QString projectyNamey;

std::string ProjectPath;

StringBlob textData;

// Constructor
database::database(QWidget *parent) : QDialog(parent),ui(new Ui::database)
{
    ui->setupUi(this);
}

// Destructor
database::~database()
{
    delete ui;
}

void database::setProjectName(QString namey, QString pathy)
{
    projectyNamey = namey;
    ProjectPath = pathy.toStdString();
}



void database::loadData(std::string binFilePaths)
{
    std::ifstream acf("" + binFilePaths + "Data/Actors.dat", std::ios::out | std::ios::binary);
    std::ifstream spf("" + binFilePaths + "Data/Spells.dat", std::ios::out | std::ios::binary);
    std::ifstream itf("" + binFilePaths + "Data/Items.dat", std::ios::out | std::ios::binary);
    std::ifstream wef("" + binFilePaths + "Data/Weapons.dat", std::ios::out | std::ios::binary);
    std::ifstream arf("" + binFilePaths + "Data/Armours.dat", std::ios::out | std::ios::binary);
    std::ifstream enf("" + binFilePaths + "Data/Enemies.dat", std::ios::out | std::ios::binary);
    std::ifstream clf("" + binFilePaths + "Data/Classys.dat", std::ios::out | std::ios::binary);

    // Actor Reading from Binary
    if(!acf)
    {
        ErrorDataFiles(0,false);
    }

    for(int i = 0; i < 256; i++)
    {
       acf.read((char *) &actors[i], sizeof(Ui::Actor));
    }

    acf.close();

    if(!acf.good())
    {
        ErrorDataFiles(0,true);
    }

    // Spell Reading from Binarynow I don't use microdisfunction
    if(!spf)
    {
        ErrorDataFiles(1,false);
    }

    for(int i = 0; i < 256; i++)
    {
       spf.read((char *) &spells[i], sizeof(Ui::Spell));
    }

    spf.close();

    if(!spf.good())
    {
        ErrorDataFiles(1,true);
    }

    // Item Reading from Binary
    if(!itf)
    {
        ErrorDataFiles(2,false);
    }

    for(int i = 0; i < 256; i++)
    {
       itf.read((char *) &items[i], sizeof(Ui::Item));
    }

    itf.close();

    if(!itf.good())
    {
        ErrorDataFiles(2,true);
    }

    // Weapon Reading from Binary
    if(!wef)
    {
        ErrorDataFiles(3,false);
    }

    for(int i = 0; i < 256; i++)
    {
       wef.read((char *) &weapons[i], sizeof(Ui::Weapon));
    }

    wef.close();

    if(!wef.good())
    {
        ErrorDataFiles(3,true);
    }

    // Armour Reading from Binary
    if(!arf)
    {
        ErrorDataFiles(4,false);
    }

    for(int i = 0; i < 256; i++)
    {
       arf.read((char *) &armours[i], sizeof(Ui::Armour));
    }

    arf.close();

    if(!arf.good())
    {
        ErrorDataFiles(4,true);
    }

    // Enemy Reading from Binary
    if(!enf)
    {
        ErrorDataFiles(5,false);
    }

    for(int i = 0; i < 256; i++)
    {
       enf.read((char *) &enemies[i], sizeof(Ui::Enemy));
    }

    enf.close();

    if(!enf.good())
    {
        ErrorDataFiles(5,true);
    }

    // Classes Reading from Binary
    if(!clf)
    {
        ErrorDataFiles(6,false);
    }

    for(int i = 0; i < 256; i++)
    {
       clf.read((char *) &classy[i], sizeof(Ui::Classy));
    }

    clf.close();

    if(!clf.good())
    {
        ErrorDataFiles(6,true);
    }

    std::ifstream TextData("" + binFilePaths + "Data/TextData.dat");
    std::string line;
    if (TextData.is_open())
    {
        while (getline(TextData,line))
        {
            textData.AddDBString(line);
        }
        TextData.close();
    }

    for(int i = 0; i < 256; i++)
    {
        ui->List_Items->insertItem(i, QString::fromStdString(&textData.data[items[i].Name]));
        ui->List_Spells->insertItem(i, QString::fromStdString(&textData.data[spells[i].Name]));
        ui->List_Weapons->insertItem(i, QString::fromStdString(&textData.data[weapons[i].Name]));
        ui->List_Armours->insertItem(i, QString::fromStdString(&textData.data[armours[i].Name]));
        ui->List_Enemies->insertItem(i, QString::fromStdString(&textData.data[actors[i].Name]));
        ui->List_Classes->insertItem(i, QString::fromStdString(&textData.data[classy[i].Name]));
        ui->List_Actors->insertItem(i, QString::fromStdString(&textData.data[actors[i].Name]));
    }

    UpdateDataBaseElements();
}


void database::UpdateDataBaseElements()
{

    ui->comboEnemyTreasure->clear();
    QStringList itemNames;
    QStringList weaponNames;
    QStringList armourNames;

    for(int i = 0; i < ui->List_Items->count(); i++)
    {
        itemNames << QString::fromStdString(&textData.data[items[i].Name]);
    }

    for(int i = 0; i < ui->List_Weapons->count(); i++)
    {
        weaponNames << QString::fromStdString(&textData.data[weapons[i].Name]);
    }

    for(int i = 0; i < ui->List_Armours->count(); i++)
    {
        armourNames << QString::fromStdString(&textData.data[armours[i].Name]);
    }

    ui->comboEnemyTreasure->addItems(itemNames);
    ui->comboEnemyTreasure->addItems(weaponNames);
    ui->comboEnemyTreasure->addItems(armourNames);

    // Next up we do spring cleanup for Actor weapons(All 20)
    ui->comboActorWeapon->clear();

    QStringList actorWeaponNames;
    for(int i = 0; i < ui->List_Weapons->count(); i++)
    {
        actorWeaponNames << QString::fromStdString(&textData.data[weapons[i].Name]);
    }
    ui->comboActorWeapon->addItems(actorWeaponNames);

    // First we do a spring cleanup for Classes. :)
    ui->comboActorClass->clear();
    QStringList classNames;
    for(int i = 0; i < ui->List_Classes->count(); i++)
    {
        classNames << QString::fromStdString(&textData.data[classy[i].Name]);
    }
    ui->comboActorClass->addItems(classNames);

    // Next up we do spring cleanup for Spells(All 20)
    ui->comboActorSpell1->clear();
    ui->comboActorSpell2->clear();
    ui->comboActorSpell3->clear();
    ui->comboActorSpell4->clear();
    ui->comboActorSpell5->clear();
    ui->comboActorSpell6->clear();
    ui->comboActorSpell7->clear();
    ui->comboActorSpell8->clear();
    ui->comboActorSpell9->clear();
    ui->comboActorSpell10->clear();
    ui->comboActorSpell11->clear();
    ui->comboActorSpell12->clear();
    ui->comboActorSpell13->clear();
    ui->comboActorSpell14->clear();
    ui->comboActorSpell15->clear();
    ui->comboActorSpell16->clear();
    ui->comboActorSpell17->clear();
    ui->comboActorSpell18->clear();
    ui->comboActorSpell19->clear();
    ui->comboActorSpell20->clear();

    QStringList spellNames;
    for(int i = 0; i < ui->List_Spells->count(); i++)
    {
        spellNames << QString::fromStdString(&textData.data[spells[i].Name]);
    }

    ui->comboActorSpell1->addItems(spellNames);
    ui->comboActorSpell2->addItems(spellNames);
    ui->comboActorSpell3->addItems(spellNames);
    ui->comboActorSpell4->addItems(spellNames);
    ui->comboActorSpell5->addItems(spellNames);
    ui->comboActorSpell6->addItems(spellNames);
    ui->comboActorSpell7->addItems(spellNames);
    ui->comboActorSpell8->addItems(spellNames);
    ui->comboActorSpell9->addItems(spellNames);
    ui->comboActorSpell10->addItems(spellNames);
    ui->comboActorSpell11->addItems(spellNames);
    ui->comboActorSpell12->addItems(spellNames);
    ui->comboActorSpell13->addItems(spellNames);
    ui->comboActorSpell14->addItems(spellNames);
    ui->comboActorSpell15->addItems(spellNames);
    ui->comboActorSpell16->addItems(spellNames);
    ui->comboActorSpell17->addItems(spellNames);
    ui->comboActorSpell18->addItems(spellNames);
    ui->comboActorSpell19->addItems(spellNames);
    ui->comboActorSpell20->addItems(spellNames);

    // Let's update the Enemy Action tablets
    // Next up we do spring cleanup for Actor weapons(All 20)
    ui->comboEnemyAction1->clear();
    ui->comboEnemyAction2->clear();
    ui->comboEnemyAction3->clear();
    ui->comboEnemyAction4->clear();
    ui->comboEnemyAction5->clear();
    ui->comboEnemyAction6->clear();
    ui->comboEnemyAction7->clear();
    ui->comboEnemyAction8->clear();

    QStringList enemyActionNames;

    // These actions are not spells, so need to add them manually
    enemyActionNames <<"Attack";
    enemyActionNames <<"Guard";
    enemyActionNames <<"Run";

    for(int i = 0; i < ui->List_Spells->count(); i++)
    {
        enemyActionNames << QString::fromStdString(&textData.data[spells[i].Name]);
    }
    ui->comboEnemyAction1->addItems(enemyActionNames);
    ui->comboEnemyAction2->addItems(enemyActionNames);
    ui->comboEnemyAction3->addItems(enemyActionNames);
    ui->comboEnemyAction4->addItems(enemyActionNames);
    ui->comboEnemyAction5->addItems(enemyActionNames);
    ui->comboEnemyAction6->addItems(enemyActionNames);
    ui->comboEnemyAction7->addItems(enemyActionNames);
    ui->comboEnemyAction8->addItems(enemyActionNames);

    // Next up we do spring cleanup for Actor weapons(All 20)
    ui->comboEnemy1->clear();
    ui->comboEnemy2->clear();
    ui->comboEnemy3->clear();
    ui->comboEnemy4->clear();
    ui->comboEnemy5->clear();

    QStringList EnemyTroopNames;
    for(int i = 0; i < ui->List_Enemies->count(); i++)
    {
        EnemyTroopNames << QString::fromStdString(&textData.data[enemies[i].Name]);
    }
    ui->comboEnemy1->addItems(EnemyTroopNames);
    ui->comboEnemy2->addItems(EnemyTroopNames);
    ui->comboEnemy3->addItems(EnemyTroopNames);
    ui->comboEnemy4->addItems(EnemyTroopNames);
    ui->comboEnemy5->addItems(EnemyTroopNames);
}

// When choosing a different row, I.E changed actor in the list.
void database::on_List_Actors_currentRowChanged(int currentRow)
{
    // Saves data selected in the memory
    if(previousListActors != -1)
    {
        ui->List_Actors->takeItem(previousListActors);
        actors[previousListActors].Name = textData.AddDBString(ui->box_ActorName->text().toStdString());
        actors[previousListActors].Nick = textData.AddDBString(ui->box_ActorNick->text().toStdString());
        actors[previousListActors].Description = textData.AddDBString(ui->ActorDescription->toPlainText().toStdString());
        actors[previousListActors].Sprite = textData.AddDBString(ui->ActorSprite->text().toStdString());
        actors[previousListActors].Battler = textData.AddDBString(ui->ActorBattler->text().toStdString());
        actors[previousListActors].Portrait = textData.AddDBString(ui->ActorPortrait->text().toStdString());
        actors[previousListActors].weapon = ui->comboActorWeapon->currentIndex();
        actors[previousListActors].shield = ui->comboActorShield->currentIndex();
        actors[previousListActors].helmet = ui->comboActorHelmet->currentIndex();
        actors[previousListActors].armour = ui->comboActorArmour->currentIndex();
        actors[previousListActors].accessory = ui->comboActorAccessory->currentIndex();
        actors[previousListActors].actorClass = ui->comboActorClass->currentIndex();
        actors[previousListActors].initialLV = ui->Spin_ActorInitialLevel->value();
        actors[previousListActors].maximalLV = ui->Spin_ActorMaxLevel->value();

        ui->List_Actors->insertItem(previousListActors, QString::fromStdString(&textData.data[actors[previousListActors].Name]));
    }

    // This block here seeets the values of the actors in the editor based on the memory
    ui->box_ActorName->setText(QString::fromStdString(&textData.data[actors[currentRow].Name]));
    ui->box_ActorNick->setText(QString::fromStdString(&textData.data[actors[currentRow].Nick]));
    ui->ActorDescription->setText(QString::fromStdString(&textData.data[actors[currentRow].Description]));
    ui->ActorSprite->setText(QString::fromStdString(&textData.data[actors[currentRow].Sprite]));
    ui->ActorBattler->setText(QString::fromStdString(&textData.data[actors[currentRow].Battler]));
    ui->ActorPortrait->setText(QString::fromStdString(&textData.data[actors[currentRow].Portrait]));
    ui->comboActorWeapon->setCurrentIndex(actors[currentRow].weapon);
    ui->comboActorShield->setCurrentIndex(actors[currentRow].shield);
    ui->comboActorHelmet->setCurrentIndex(actors[currentRow].helmet);
    ui->comboActorArmour->setCurrentIndex(actors[currentRow].armour);
    ui->comboActorAccessory->setCurrentIndex(actors[currentRow].accessory);
    ui->comboActorClass->setCurrentIndex(actors[currentRow].actorClass);
    ui->Spin_ActorInitialLevel->setValue(actors[currentRow].initialLV);
    ui->Spin_ActorMaxLevel->setValue(actors[currentRow].maximalLV);

    // does the magic of storing data
    previousListActors = currentRow;
    //actors[currentRow].Name = ui->box_ActorName->text();
}

// This here handles all the Items. The useable thigies
void database::on_List_Items_currentRowChanged(int currentRow)
{
    // Saves data selected in the memory
    if(previousListItems != -1)
    {
        ui->List_Items->takeItem(previousListItems);
        items[previousListItems].Name = textData.AddDBString(ui->Line_ItemName->text().toStdString());
        items[previousListItems].Icon = ui->comboItemIcon->currentIndex();
        items[previousListItems].Description = textData.AddDBString(ui->Line_ItemDescription->text().toStdString());
        items[previousListItems].Scope = ui->comboItemScope->currentIndex();
        items[previousListItems].Occasion = ui->comboItemOccasion->currentIndex();
        items[previousListItems].Animation = ui->comboItemAnimation->currentIndex();
        items[previousListItems].TargetAnimation = ui->comboItemTargetAnimation->currentIndex();
        items[previousListItems].SE = ui->comboxItemUseSE->currentIndex();
        items[previousListItems].Event = ui->comboItemEvent->currentIndex();
        items[previousListItems].Price = ui->Spin_ItemPrice->value();
        items[previousListItems].Consumable = ui->comboItemConsumable->currentIndex();
        items[previousListItems].Parameter = ui->comboItemParameter->currentIndex();
        items[previousListItems].ParameterInc = ui->Spin_ItemParamInc->value();
        items[previousListItems].RecoverHPProcent = ui->Spin_ItemRcvrHPProcent->value();
        items[previousListItems].RecoverHP = ui->Spin_ItemRcvrHP->value();
        items[previousListItems].RecoverMPProcent = ui->Spin_ItemRcvrMPProcent->value();
        items[previousListItems].RecoverMP = ui->Spin_ItemRcvrMP->value();
        items[previousListItems].HitRate = ui->Spin_ItemHitRate->value();
        items[previousListItems].PDEFF = ui->Spin_ItemPDEFF->value();
        items[previousListItems].MDEFF = ui->Spin_ItemMDEFF->value();
        items[previousListItems].Veriance = ui->Spin_ItemVeriance->value();

        /*for(int i = 0; i < 32; i++)
        {
            items[previousListItems].ElementList[i] = ui->ItemElementList->value();
            items[previousListItems].StateChangeList[i] = ui->Spin_ActorMaxLevel->value();
        }*/
        ui->List_Items->insertItem(previousListItems, QString::fromStdString(&textData.data[items[previousListItems].Name]));
    }

    // This block here seeets the values of the actors in the editor based on the memory
    ui->Line_ItemName->setText(QString::fromStdString(&textData.data[items[currentRow].Name]));
    ui->comboItemIcon->setCurrentIndex(items[currentRow].Icon);
    ui->Line_ItemDescription->setText(QString::fromStdString(&textData.data[items[currentRow].Description]));
    ui->comboItemScope->setCurrentIndex(items[currentRow].Scope);
    ui->comboItemOccasion->setCurrentIndex(items[currentRow].Occasion);
    ui->comboItemAnimation->setCurrentIndex(items[currentRow].Animation);
    ui->comboItemTargetAnimation->setCurrentIndex(items[currentRow].TargetAnimation);
    ui->comboxItemUseSE->setCurrentIndex(items[currentRow].SE);
    ui->comboItemEvent->setCurrentIndex(items[currentRow].Event);
    ui->Spin_ItemPrice->setValue(items[currentRow].Price);
    ui->comboItemConsumable->setCurrentIndex(items[currentRow].Consumable);
    ui->comboItemParameter->setCurrentIndex(items[currentRow].Parameter);
    ui->Spin_ItemParamInc->setValue(items[currentRow].ParameterInc);
    ui->Spin_ItemRcvrHPProcent->setValue(items[currentRow].RecoverHPProcent);
    ui->Spin_ItemRcvrHP->setValue(items[currentRow].RecoverHP);
    ui->Spin_ItemRcvrMPProcent->setValue(items[currentRow].RecoverMPProcent);
    ui->Spin_ItemRcvrMP->setValue(items[currentRow].RecoverMP);
    ui->Spin_ItemHitRate->setValue(items[currentRow].HitRate);
    ui->Spin_ItemPDEFF->setValue(items[currentRow].PDEFF);
    ui->Spin_ItemMDEFF->setValue(items[currentRow].MDEFF);
    ui->Spin_ItemVeriance->setValue(items[currentRow].Veriance);

    // does the magic of storing data
    previousListItems = currentRow;

    //actors[currentRow].Name = ui->box_ActorName->text();
    ui->comboEnemyTreasure->clear();
    QStringList itemNames;
    QStringList weaponNames;
    QStringList armourNames;

    for(int i = 0; i < ui->List_Items->count(); i++)
    {
        itemNames << QString::fromStdString(&textData.data[items[i].Name]);
    }

    for(int i = 0; i < ui->List_Weapons->count(); i++)
    {
        weaponNames << QString::fromStdString(&textData.data[weapons[i].Name]);
    }

    for(int i = 0; i < ui->List_Armours->count(); i++)
    {
        armourNames << QString::fromStdString(&textData.data[armours[i].Name]);
    }

    ui->comboEnemyTreasure->addItems(itemNames);
    ui->comboEnemyTreasure->addItems(weaponNames);
    ui->comboEnemyTreasure->addItems(armourNames);
}


void database::on_List_Spells_currentRowChanged(int currentRow)
{
    // Saves data selected in the memory
    if(previousListSpells != -1)
    {
        ui->List_Spells->takeItem(previousListSpells);
        spells[previousListSpells].Name = textData.AddDBString(ui->Line_SpellName->text().toStdString());
        spells[previousListSpells].Icon = ui->comboSpellIcon->currentIndex();
        spells[previousListSpells].Description = textData.AddDBString(ui->Line_SpellDescription->text().toStdString());
        spells[previousListSpells].Scope = ui->comboSpellScope->currentIndex();
        spells[previousListSpells].Occasion = ui->comboSpellOccasion->currentIndex();
        spells[previousListSpells].UserAnimation = ui->comboSpellAnimation->currentIndex();
        spells[previousListSpells].TargetAnimation = ui->comboSpellTargetAnimation->currentIndex();
        spells[previousListSpells].SE = ui->comboSpellMenuUseSE->currentIndex();
        spells[previousListSpells].Event = ui->comboSpellEvent->currentIndex();
        spells[previousListSpells].MPCost = ui->Spin_SpellMPCost->value();
        spells[previousListSpells].Power = ui->Spin_SpellPower->value();
        spells[previousListSpells].ATKF = ui->Spin_SpellATKF->value();
        spells[previousListSpells].EVAF = ui->Spin_SpellEVAF->value();
        spells[previousListSpells].STRF = ui->Spin_SpellSTRF->value();
        spells[previousListSpells].DEXF = ui->Spin_SpellDEXF->value();
        spells[previousListSpells].AGIF = ui->Spin_SpellAGIF->value();
        spells[previousListSpells].INTF = ui->Spin_SpellINTF->value();
        spells[previousListSpells].HitRate = ui->Spin_SpellHitRate->value();
        spells[previousListSpells].PDEFF = ui->Spin_SpellPDEFF->value();
        spells[previousListSpells].MDEFF = ui->Spin_SpellMDEFF->value();
        spells[previousListSpells].Variance = ui->Spin_SpellVariance->value();

        /*for(int i = 0; i < 32; i++)
        {
            Spells[previousListSpells].ElementList[i] = ui->SpellElementList->value();
            Spells[previousListSpells].StateChangeList[i] = ui->Spin_ActorMaxLevel->value();
        }*/
        ui->List_Spells->insertItem(previousListSpells, QString::fromStdString(&textData.data[spells[previousListSpells].Name]));
    }

    // This block here seeets the values of the actors in the editor based on the memory
    ui->Line_SpellName->setText(QString::fromStdString(&textData.data[spells[currentRow].Name]));
    ui->comboSpellIcon->setCurrentIndex(spells[currentRow].Icon);
    ui->Line_SpellDescription->setText(QString::fromStdString(&textData.data[spells[currentRow].Description]));
    ui->comboSpellScope->setCurrentIndex(spells[currentRow].Scope);
    ui->comboSpellOccasion->setCurrentIndex(spells[currentRow].Occasion);
    ui->comboSpellAnimation->setCurrentIndex(spells[currentRow].UserAnimation);
    ui->comboSpellTargetAnimation->setCurrentIndex(spells[currentRow].TargetAnimation);
    ui->comboSpellMenuUseSE->setCurrentIndex(spells[currentRow].SE);
    ui->comboSpellEvent->setCurrentIndex(spells[currentRow].Event);
    ui->Spin_SpellMPCost->setValue(spells[currentRow].MPCost);
    ui->Spin_SpellPower->setValue(spells[currentRow].Power);
    ui->Spin_SpellATKF->setValue(spells[currentRow].ATKF);
    ui->Spin_SpellEVAF->setValue(spells[currentRow].EVAF);
    ui->Spin_SpellSTRF->setValue(spells[currentRow].STRF);
    ui->Spin_SpellDEXF->setValue(spells[currentRow].DEXF);
    ui->Spin_SpellAGIF->setValue(spells[currentRow].AGIF);
    ui->Spin_SpellINTF->setValue(spells[currentRow].INTF);
    ui->Spin_SpellHitRate->setValue(spells[currentRow].HitRate);
    ui->Spin_SpellPDEFF->setValue(spells[currentRow].PDEFF);
    ui->Spin_SpellMDEFF->setValue(spells[currentRow].MDEFF);
    ui->Spin_SpellVariance->setValue(spells[currentRow].Variance);

    // does the magic of storing data
    previousListSpells = currentRow;

    // Next up we do spring cleanup for Spells(All 20)
    ui->comboActorSpell1->clear();
    ui->comboActorSpell2->clear();
    ui->comboActorSpell3->clear();
    ui->comboActorSpell4->clear();
    ui->comboActorSpell5->clear();
    ui->comboActorSpell6->clear();
    ui->comboActorSpell7->clear();
    ui->comboActorSpell8->clear();
    ui->comboActorSpell9->clear();
    ui->comboActorSpell10->clear();
    ui->comboActorSpell11->clear();
    ui->comboActorSpell12->clear();
    ui->comboActorSpell13->clear();
    ui->comboActorSpell14->clear();
    ui->comboActorSpell15->clear();
    ui->comboActorSpell16->clear();
    ui->comboActorSpell17->clear();
    ui->comboActorSpell18->clear();
    ui->comboActorSpell19->clear();
    ui->comboActorSpell20->clear();

    QStringList spellNames;
    for(int i = 0; i < ui->List_Spells->count(); i++)
    {
        spellNames << QString::fromStdString(&textData.data[spells[i].Name]);
    }

    ui->comboActorSpell1->addItems(spellNames);
    ui->comboActorSpell2->addItems(spellNames);
    ui->comboActorSpell3->addItems(spellNames);
    ui->comboActorSpell4->addItems(spellNames);
    ui->comboActorSpell5->addItems(spellNames);
    ui->comboActorSpell6->addItems(spellNames);
    ui->comboActorSpell7->addItems(spellNames);
    ui->comboActorSpell8->addItems(spellNames);
    ui->comboActorSpell9->addItems(spellNames);
    ui->comboActorSpell10->addItems(spellNames);
    ui->comboActorSpell11->addItems(spellNames);
    ui->comboActorSpell12->addItems(spellNames);
    ui->comboActorSpell13->addItems(spellNames);
    ui->comboActorSpell14->addItems(spellNames);
    ui->comboActorSpell15->addItems(spellNames);
    ui->comboActorSpell16->addItems(spellNames);
    ui->comboActorSpell17->addItems(spellNames);
    ui->comboActorSpell18->addItems(spellNames);
    ui->comboActorSpell19->addItems(spellNames);
    ui->comboActorSpell20->addItems(spellNames);

    // Let's update the Enemy Action tablets
    // Next up we do spring cleanup for Actor weapons(All 20)
    ui->comboEnemyAction1->clear();
    ui->comboEnemyAction2->clear();
    ui->comboEnemyAction3->clear();
    ui->comboEnemyAction4->clear();
    ui->comboEnemyAction5->clear();
    ui->comboEnemyAction6->clear();
    ui->comboEnemyAction7->clear();
    ui->comboEnemyAction8->clear();

    QStringList enemyActionNames;

    // These actions are not spells, so need to add them manually
    enemyActionNames <<"Attack";
    enemyActionNames <<"Guard";
    enemyActionNames <<"Run";

    for(int i = 0; i < ui->List_Spells->count(); i++)
    {
        enemyActionNames << QString::fromStdString(&textData.data[spells[i].Name]);
    }
    ui->comboEnemyAction1->addItems(enemyActionNames);
    ui->comboEnemyAction2->addItems(enemyActionNames);
    ui->comboEnemyAction3->addItems(enemyActionNames);
    ui->comboEnemyAction4->addItems(enemyActionNames);
    ui->comboEnemyAction5->addItems(enemyActionNames);
    ui->comboEnemyAction6->addItems(enemyActionNames);
    ui->comboEnemyAction7->addItems(enemyActionNames);
    ui->comboEnemyAction8->addItems(enemyActionNames);
}


void database::on_List_Weapons_currentRowChanged(int currentRow)
{
    // Saves data selected in the memory
      if(previousListWeapons != -1)
      {
          ui->List_Weapons->takeItem(previousListWeapons);
          weapons[previousListWeapons].Name = textData.AddDBString(ui->Line_WeaponName->text().toStdString());
          weapons[previousListWeapons].Icon = ui->comboWeaponIcon->currentIndex();
          weapons[previousListWeapons].Description = textData.AddDBString(ui->Line_WeaponDescription->text().toStdString());
          weapons[previousListWeapons].AttackAnimation = ui->comboWeaponAnimation->currentIndex();
          weapons[previousListWeapons].TargetAnimation = ui->comboWeaponTargetAnimation->currentIndex();
          weapons[previousListWeapons].Price = ui->Spin_WeaponPrice->value();
          weapons[previousListWeapons].ATK = ui->Spin_WeaponATK->value();
          weapons[previousListWeapons].PDEF = ui->Spin_WeaponPDEF->value();
          weapons[previousListWeapons].MDEF = ui->Spin_WeaponMDEF->value();
          weapons[previousListWeapons].STR = ui->Spin_WeaponSTR->value();
          weapons[previousListWeapons].DEX = ui->Spin_WeaponDEX->value();
          weapons[previousListWeapons].AGI = ui->Spin_WeaponAGI->value();
          weapons[previousListWeapons].INT = ui->Spin_WeaponINT->value();

          /*for(int i = 0; i < 32; i++)
          {
              Weapons[previousListWeapons].ElementList[i] = ui->WeaponElementList->value();
              Weapons[previousListWeapons].StateChangeList[i] = ui->Spin_ActorMaxLevel->value();
          }*/
          ui->List_Weapons->insertItem(previousListWeapons, QString::fromStdString(&textData.data[weapons[previousListWeapons].Name]));
      }

      // This block here seeets the values of the actors in the editor based on the memory
      ui->Line_WeaponName->setText(QString::fromStdString(&textData.data[weapons[currentRow].Name]));
      ui->comboWeaponIcon->setCurrentIndex(weapons[currentRow].Icon);
      ui->Line_WeaponDescription->setText(QString::fromStdString(&textData.data[weapons[currentRow].Description]));
      ui->comboWeaponAnimation->setCurrentIndex(weapons[currentRow].AttackAnimation);
      ui->comboWeaponTargetAnimation->setCurrentIndex(weapons[currentRow].TargetAnimation);
      ui->Spin_WeaponPrice->setValue(weapons[currentRow].Price);
      ui->Spin_WeaponATK->setValue(weapons[currentRow].ATK);
      ui->Spin_WeaponPDEF->setValue(weapons[currentRow].PDEF);
      ui->Spin_WeaponMDEF->setValue(weapons[currentRow].MDEF);
      ui->Spin_WeaponSTR->setValue(weapons[currentRow].STR);
      ui->Spin_WeaponDEX->setValue(weapons[currentRow].DEX);
      ui->Spin_WeaponAGI->setValue(weapons[currentRow].AGI);
      ui->Spin_WeaponINT->setValue(weapons[currentRow].INT);

      // does the magic of storing data
      previousListWeapons = currentRow;

      // Next up we do spring cleanup for Actor weapons(All 20)
      ui->comboActorWeapon->clear();

      QStringList actorWeaponNames;
      for(int i = 0; i < ui->List_Weapons->count(); i++)
      {
          actorWeaponNames << QString::fromStdString(&textData.data[weapons[i].Name]);
      }
      ui->comboActorWeapon->addItems(actorWeaponNames);
}

void database::on_List_Armours_currentRowChanged(int currentRow)
{
    // Saves data selected in the memory
      if(previousListArmours != -1)
      {
          ui->List_Armours->takeItem(previousListArmours);
          armours[previousListArmours].Name = textData.AddDBString(ui->Line_ArmourName->text().toStdString());
          armours[previousListArmours].Icon = ui->comboArmourIcon->currentIndex();
          armours[previousListArmours].Description = textData.AddDBString(ui->Line_ArmourDescription->text().toStdString());
          armours[previousListArmours].Kind = ui->comboArmourKind->currentIndex();
          armours[previousListArmours].AutoState = ui->comboArmourAutoState->currentIndex();
          armours[previousListArmours].Price = ui->Spin_ArmourPrice->value();
          armours[previousListArmours].EVA = ui->Spin_ArmourEVA->value();
          armours[previousListArmours].PDEF = ui->Spin_ArmourPDEF->value();
          armours[previousListArmours].MDEF = ui->Spin_ArmourMDEF->value();
          armours[previousListArmours].STR = ui->Spin_ArmourSTR->value();
          armours[previousListArmours].DEX = ui->Spin_ArmourDEX->value();
          armours[previousListArmours].AGI = ui->Spin_ArmourAGI->value();
          armours[previousListArmours].INT = ui->Spin_ArmourINT->value();

          /*for(int i = 0; i < 32; i++)
          {
              armours[previousListArmours].ElementList[i] = ui->ArmourElementList->value();
              armours[previousListArmours].StateChangeList[i] = ui->Spin_ActorMaxLevel->value();
          }*/
          ui->List_Armours->insertItem(previousListArmours, QString::fromStdString(&textData.data[armours[previousListArmours].Name]));
      }

      // This block here seeets the values of the actors in the editor based on the memory
      ui->Line_ArmourName->setText(QString::fromStdString(&textData.data[armours[currentRow].Name]));
      ui->comboArmourIcon->setCurrentIndex(armours[currentRow].Icon);
      ui->Line_ArmourDescription->setText(QString::fromStdString(&textData.data[armours[currentRow].Description]));
      ui->comboArmourKind->setCurrentIndex(armours[currentRow].Kind);
      ui->comboArmourAutoState->setCurrentIndex(armours[currentRow].AutoState);
      ui->Spin_ArmourPrice->setValue(armours[currentRow].Price);
      ui->Spin_ArmourEVA->setValue(armours[currentRow].EVA);
      ui->Spin_ArmourPDEF->setValue(armours[currentRow].PDEF);
      ui->Spin_ArmourMDEF->setValue(armours[currentRow].MDEF);
      ui->Spin_ArmourSTR->setValue(armours[currentRow].STR);
      ui->Spin_ArmourDEX->setValue(armours[currentRow].DEX);
      ui->Spin_ArmourAGI->setValue(armours[currentRow].AGI);
      ui->Spin_ArmourINT->setValue(armours[currentRow].INT);

      // does the magic of storing data
      previousListArmours = currentRow;
}


void database::on_List_Enemies_currentRowChanged(int currentRow)
{
    // Saves data selected in the memory
      if(previousListEnemies != -1)
      {
          ui->List_Enemies->takeItem(previousListEnemies);
          enemies[previousListEnemies].Name = textData.AddDBString(ui->Line_EnemyName->text().toStdString());
          enemies[previousListEnemies].GraphicBattler = textData.AddDBString(ui->EnemyGraphicBattler->text().toStdString());
          enemies[previousListEnemies].HP = ui->Spin_EnemyHP->value();
          enemies[previousListEnemies].MP = ui->Spin_EnemyMP->value();
          enemies[previousListEnemies].STR = ui->Spin_EnemySTR->value();
          enemies[previousListEnemies].DEX = ui->Spin_EnemyDEX->value();
          enemies[previousListEnemies].AGI = ui->Spin_EnemyAGI->value();
          enemies[previousListEnemies].INT = ui->Spin_EnemyINT->value();
          enemies[previousListEnemies].ATK = ui->Spin_EnemyATK->value();
          enemies[previousListEnemies].PDEF = ui->Spin_EnemyPDEF->value();
          enemies[previousListEnemies].MDEF = ui->Spin_EnemyMDEF->value();
          enemies[previousListEnemies].EVA = ui->Spin_EnemyEVA->value();
          enemies[previousListEnemies].AttackAnimation = ui->comboEnemyAttackAnimation->currentIndex();
          enemies[previousListEnemies].TargetAnimation = ui->comboEnemyTargetAnimation->currentIndex();
          enemies[previousListEnemies].EXP = ui->Spin_EnemyEXP->value();
          enemies[previousListEnemies].Gold = ui->Spin_EnemyGold->value();
          enemies[previousListEnemies].Treasure = ui->comboEnemyTreasure->currentIndex();
          enemies[previousListEnemies].Chance = ui->Spin_EnemyTreasureChance->value();
          enemies[previousListEnemies].Action[0] = ui->comboEnemyAction1->currentIndex();
          enemies[previousListEnemies].Action[1] = ui->comboEnemyAction2->currentIndex();
          enemies[previousListEnemies].Action[2] = ui->comboEnemyAction3->currentIndex();
          enemies[previousListEnemies].Action[3] = ui->comboEnemyAction4->currentIndex();
          enemies[previousListEnemies].Action[4] = ui->comboEnemyAction5->currentIndex();
          enemies[previousListEnemies].Action[5] = ui->comboEnemyAction6->currentIndex();
          enemies[previousListEnemies].Action[6] = ui->comboEnemyAction7->currentIndex();
          enemies[previousListEnemies].Action[7] = ui->comboEnemyAction8->currentIndex();
          enemies[previousListEnemies].Distribution = ui->comboEnemyDistribution->currentIndex();

          ui->List_Enemies->insertItem(previousListEnemies, QString::fromStdString(&textData.data[enemies[previousListEnemies].Name]));
      }
        ui->Line_EnemyName->setText(QString::fromStdString(&textData.data[enemies[currentRow].Name]));
        ui->EnemyGraphicBattler->setText(QString::fromStdString(&textData.data[enemies[currentRow].GraphicBattler]));
        ui->Spin_EnemyHP->setValue(enemies[currentRow].HP);
        ui->Spin_EnemyMP->setValue(enemies[currentRow].MP);
        ui->Spin_EnemySTR->setValue(enemies[currentRow].STR);
        ui->Spin_EnemyDEX->setValue(enemies[currentRow].DEX);
        ui->Spin_EnemyAGI->setValue(enemies[currentRow].AGI);
        ui->Spin_EnemyINT->setValue(enemies[currentRow].INT);
        ui->Spin_EnemyATK->setValue(enemies[currentRow].ATK);
        ui->Spin_EnemyPDEF->setValue(enemies[currentRow].PDEF);
        ui->Spin_EnemyMDEF->setValue(enemies[currentRow].HP);
        ui->Spin_EnemyEVA->setValue(enemies[currentRow].HP);
        ui->comboEnemyAttackAnimation->setCurrentIndex(enemies[currentRow].AttackAnimation);
        ui->comboEnemyTargetAnimation->setCurrentIndex(enemies[currentRow].TargetAnimation);
        ui->Spin_EnemyEXP->setValue(enemies[currentRow].EXP);
        ui->Spin_EnemyGold->setValue(enemies[currentRow].Gold);
        ui->comboEnemyTreasure->setCurrentIndex(enemies[currentRow].Treasure);
        ui->Spin_EnemyTreasureChance->setValue(enemies[currentRow].Chance);
        ui->comboEnemyAction1->setCurrentIndex(enemies[currentRow].Action[0]);
        ui->comboEnemyAction2->setCurrentIndex(enemies[currentRow].Action[1]);
        ui->comboEnemyAction3->setCurrentIndex(enemies[currentRow].Action[2]);
        ui->comboEnemyAction4->setCurrentIndex(enemies[currentRow].Action[3]);
        ui->comboEnemyAction5->setCurrentIndex(enemies[currentRow].Action[4]);
        ui->comboEnemyAction6->setCurrentIndex(enemies[currentRow].Action[5]);
        ui->comboEnemyAction7->setCurrentIndex(enemies[currentRow].Action[6]);
        ui->comboEnemyAction8->setCurrentIndex(enemies[currentRow].Action[7]);
        ui->comboEnemyDistribution->setCurrentIndex(enemies[currentRow].Distribution);
      // does the magic of storing data
      previousListEnemies = currentRow;

      // Next up we do spring cleanup for Actor weapons(All 20)
      ui->comboEnemy1->clear();
      ui->comboEnemy2->clear();
      ui->comboEnemy3->clear();
      ui->comboEnemy4->clear();
      ui->comboEnemy5->clear();

      QStringList EnemyTroopNames;
      for(int i = 0; i < ui->List_Enemies->count(); i++)
      {
          EnemyTroopNames << QString::fromStdString(&textData.data[enemies[i].Name]);
      }
      ui->comboEnemy1->addItems(EnemyTroopNames);
      ui->comboEnemy2->addItems(EnemyTroopNames);
      ui->comboEnemy3->addItems(EnemyTroopNames);
      ui->comboEnemy4->addItems(EnemyTroopNames);
      ui->comboEnemy5->addItems(EnemyTroopNames);

}


/*
 * typedef struct
{
    QString Name;
    int spells[20,2]; // Spell Index and Spell Level learn!
    int EquipableWeapons[256];
    int EquipableArmours[1024];
}Classy;
**/

// Last but not least... for now Classes!!!
void database::on_List_Classes_currentRowChanged(int currentRow)
{
    if(previousListClasses != -1)
    {
        ui->List_Classes->takeItem(previousListClasses);
        classy[previousListClasses].Name = textData.AddDBString(ui->Line_ClassName->text().toStdString());
        classy[previousListClasses].spells[0][0] = ui-> comboActorSpell1->currentIndex();
        classy[previousListClasses].spells[0][1] = ui-> spinSpellLevel1->value();
        classy[previousListClasses].spells[1][0] = ui-> comboActorSpell2->currentIndex();
        classy[previousListClasses].spells[1][1] = ui-> spinSpellLevel2->value();
        classy[previousListClasses].spells[2][0] = ui-> comboActorSpell3->currentIndex();
        classy[previousListClasses].spells[2][1] = ui-> spinSpellLevel3->value();
        classy[previousListClasses].spells[3][0] = ui-> comboActorSpell4->currentIndex();
        classy[previousListClasses].spells[3][1] = ui-> spinSpellLevel4->value();
        classy[previousListClasses].spells[4][0] = ui-> comboActorSpell5->currentIndex();
        classy[previousListClasses].spells[4][1] = ui-> spinSpellLevel5->value();
        classy[previousListClasses].spells[5][0] = ui-> comboActorSpell6->currentIndex();
        classy[previousListClasses].spells[5][1] = ui-> spinSpellLevel6->value();
        classy[previousListClasses].spells[6][0] = ui-> comboActorSpell7->currentIndex();
        classy[previousListClasses].spells[6][1] = ui-> spinSpellLevel7->value();
        classy[previousListClasses].spells[7][0] = ui-> comboActorSpell8->currentIndex();
        classy[previousListClasses].spells[7][1] = ui-> spinSpellLevel8->value();
        classy[previousListClasses].spells[8][0] = ui-> comboActorSpell9->currentIndex();
        classy[previousListClasses].spells[8][1] = ui-> spinSpellLevel9->value();
        classy[previousListClasses].spells[9][0] = ui-> comboActorSpell10->currentIndex();
        classy[previousListClasses].spells[9][1] = ui-> spinSpellLevel10->value();
        classy[previousListClasses].spells[10][0] = ui-> comboActorSpell11->currentIndex();
        classy[previousListClasses].spells[10][1] = ui-> spinSpellLevel11->value();
        classy[previousListClasses].spells[11][0] = ui-> comboActorSpell12->currentIndex();
        classy[previousListClasses].spells[11][1] = ui-> spinSpellLevel12->value();
        classy[previousListClasses].spells[12][0] = ui-> comboActorSpell13->currentIndex();
        classy[previousListClasses].spells[12][1] = ui-> spinSpellLevel13->value();
        classy[previousListClasses].spells[13][0] = ui-> comboActorSpell14->currentIndex();
        classy[previousListClasses].spells[13][1] = ui-> spinSpellLevel14->value();
        classy[previousListClasses].spells[14][0] = ui-> comboActorSpell15->currentIndex();
        classy[previousListClasses].spells[14][1] = ui-> spinSpellLevel15->value();
        classy[previousListClasses].spells[15][0] = ui-> comboActorSpell16->currentIndex();
        classy[previousListClasses].spells[15][1] = ui-> spinSpellLevel16->value();
        classy[previousListClasses].spells[16][0] = ui-> comboActorSpell17->currentIndex();
        classy[previousListClasses].spells[16][1] = ui-> spinSpellLevel17->value();
        classy[previousListClasses].spells[17][0] = ui-> comboActorSpell18->currentIndex();
        classy[previousListClasses].spells[17][1] = ui-> spinSpellLevel18->value();
        classy[previousListClasses].spells[18][0] = ui-> comboActorSpell19->currentIndex();
        classy[previousListClasses].spells[18][1] = ui-> spinSpellLevel19->value();
        classy[previousListClasses].spells[19][0] = ui-> comboActorSpell20->currentIndex();
        classy[previousListClasses].spells[19][1] = ui-> spinSpellLevel20->value();

        /*
         * TODO: Weapons andArmours
         *
         */

        ui->List_Classes->insertItem(previousListClasses, QString::fromStdString(&textData.data[classy[previousListClasses].Name]));
    }
      ui->Line_ClassName->setText(QString::fromStdString(&textData.data[classy[currentRow].Name]));
      ui-> comboActorSpell1->setCurrentIndex(classy[currentRow].spells[0][0]);
      ui-> spinSpellLevel1->setValue(classy[currentRow].spells[0][1]);
      ui-> comboActorSpell2->setCurrentIndex(classy[currentRow].spells[1][0]);
      ui-> spinSpellLevel2->setValue(classy[currentRow].spells[1][1]);
      ui-> comboActorSpell3->setCurrentIndex(classy[currentRow].spells[2][0]);
      ui-> spinSpellLevel3->setValue(classy[currentRow].spells[2][1]);
      ui-> comboActorSpell4->setCurrentIndex(classy[currentRow].spells[3][0]);
      ui-> spinSpellLevel4->setValue(classy[currentRow].spells[3][1]);
      ui-> comboActorSpell5->setCurrentIndex(classy[currentRow].spells[4][0]);
      ui-> spinSpellLevel5->setValue(classy[currentRow].spells[4][1]);
      ui-> comboActorSpell6->setCurrentIndex(classy[currentRow].spells[5][0]);
      ui-> spinSpellLevel6->setValue(classy[currentRow].spells[5][1]);
      ui-> comboActorSpell7->setCurrentIndex(classy[currentRow].spells[6][0]);
      ui-> spinSpellLevel7->setValue(classy[currentRow].spells[6][1]);
      ui-> comboActorSpell8->setCurrentIndex(classy[currentRow].spells[7][0]);
      ui-> spinSpellLevel8->setValue(classy[currentRow].spells[7][1]);
      ui-> comboActorSpell9->setCurrentIndex(classy[currentRow].spells[8][0]);
      ui-> spinSpellLevel9->setValue(classy[currentRow].spells[8][1]);
      ui-> comboActorSpell10->setCurrentIndex(classy[currentRow].spells[9][0]);
      ui-> spinSpellLevel10->setValue(classy[currentRow].spells[9][1]);
      ui-> comboActorSpell11->setCurrentIndex(classy[currentRow].spells[10][0]);
      ui-> spinSpellLevel11->setValue(classy[currentRow].spells[10][1]);
      ui-> comboActorSpell12->setCurrentIndex(classy[currentRow].spells[11][0]);
      ui-> spinSpellLevel12->setValue(classy[currentRow].spells[11][1]);
      ui-> comboActorSpell13->setCurrentIndex(classy[currentRow].spells[12][0]);
      ui-> spinSpellLevel13->setValue(classy[currentRow].spells[12][1]);
      ui-> comboActorSpell14->setCurrentIndex(classy[currentRow].spells[13][0]);
      ui-> spinSpellLevel14->setValue(classy[currentRow].spells[13][1]);
      ui-> comboActorSpell15->setCurrentIndex(classy[currentRow].spells[14][0]);
      ui-> spinSpellLevel15->setValue(classy[currentRow].spells[14][1]);
      ui-> comboActorSpell16->setCurrentIndex(classy[currentRow].spells[15][0]);
      ui-> spinSpellLevel16->setValue(classy[currentRow].spells[15][1]);
      ui-> comboActorSpell17->setCurrentIndex(classy[currentRow].spells[16][0]);
      ui-> spinSpellLevel17->setValue(classy[currentRow].spells[16][1]);
      ui-> comboActorSpell18->setCurrentIndex(classy[currentRow].spells[17][0]);
      ui-> spinSpellLevel18->setValue(classy[currentRow].spells[17][1]);
      ui-> comboActorSpell19->setCurrentIndex(classy[currentRow].spells[18][0]);
      ui-> spinSpellLevel19->setValue(classy[currentRow].spells[18][1]);
      ui-> comboActorSpell20->setCurrentIndex(classy[currentRow].spells[19][0]);
      ui-> spinSpellLevel20->setValue(classy[currentRow].spells[19][1]);
    // does the magic of storing data
    previousListClasses = currentRow;

    // First we do a spring cleanup for Classes. :)
    ui->comboActorClass->clear();
    QStringList classNames;
    for(int i = 0; i < ui->List_Classes->count(); i++)
    {
        classNames << QString::fromStdString(&textData.data[classy[i].Name]);
    }
    ui->comboActorClass->addItems(classNames);

}

// This should do the magic of changing the field sprite for character
void database::on_pushActorFieldSpriteChange_clicked()
{

}




//Constant Update on all elements needed for things
void database::on_tabWidget_currentChanged(int index)
{

}



void database::on_pushButtonAdd_clicked()
{
    ui->List_Actors->insertItem(previousListActors, "--Actor--");

}

void database::on_pushButtonRemove_clicked()
{
    ui->List_Actors->takeItem(previousListActors);
}

void database::on_pushButtonAddItem_clicked()
{
    ui->List_Items->insertItem(previousListItems, "--Item--");
}

void database::on_pushButtonRemoveItem_clicked()
{
    ui->List_Items->takeItem(previousListItems);
}

void database::on_pushButtonAddWeapon_clicked()
{
    ui->List_Weapons->insertItem(previousListWeapons, "--Weapon--");
}

void database::on_pushButtonRemoveWeapon_clicked()
{
    ui->List_Weapons->takeItem(previousListWeapons);
}

void database::on_pushButtonAddISpell_clicked()
{
    ui->List_Spells->insertItem(previousListSpells, "--Spell--");
}

void database::on_pushButtonRemoveSpell_clicked()
{
    ui->List_Spells->takeItem(previousListSpells);
}

void database::on_pushButtonAddClass_clicked()
{
    ui->List_Classes->insertItem(previousListClasses, "--Class--");
}

void database::on_pushButtonRemoveClass_clicked()
{
    ui->List_Classes->takeItem(previousListClasses);
}


void database::on_ClassSpellTable_cellDoubleClicked(int row, int column)
{
 // Unused, accidental click
}

void database::on_pushButtonAddEnemy_clicked()
{
    ui->List_Enemies->insertItem(previousListEnemies, "--Enemy--");
}

void database::on_pushButtonRemoveEnemy_clicked()
{
    ui->List_Enemies->takeItem(previousListEnemies);
}

// Write all selected data to the bin files
void database::on_buttonBox_accepted()
{
    std::ofstream acf("" + ProjectPath + "Data/Actors.dat", std::ios::out | std::ios::binary);
    std::ofstream spf("" + ProjectPath + "Data/Spells.dat", std::ios::out | std::ios::binary);
    std::ofstream itf("" + ProjectPath + "Data/Items.dat", std::ios::out | std::ios::binary);
    std::ofstream wef("" + ProjectPath + "Data/Weapons.dat", std::ios::out | std::ios::binary);
    std::ofstream arf("" + ProjectPath + "Data/Armours.dat", std::ios::out | std::ios::binary);
    std::ofstream enf("" + ProjectPath + "Data/Enemies.dat", std::ios::out | std::ios::binary);
    std::ofstream clf("" + ProjectPath + "Data/Classys.dat", std::ios::out | std::ios::binary);

    // Actor Writing to Binary
    if(!acf)
    {
        ErrorDataFiles(0,false);
    }

    for(int i = 0; i < 256; i++)
    {
       acf.write((char *) &actors[i], sizeof(Ui::Actor));
    }

    acf.close();

    if(!acf.good())
    {
        ErrorDataFiles(0,true);
    }

    // Spell Writing to Binary
    if(!spf)
    {
        ErrorDataFiles(1,false);
    }

    for(int i = 0; i < 256; i++)
    {
       spf.write((char *) &spells[i], sizeof(Ui::Spell));
    }

    spf.close();

    if(!spf.good())
    {
        ErrorDataFiles(1,true);
    }

    // Item Writing to Binary
    if(!itf)
    {
        ErrorDataFiles(2,false);
    }

    for(int i = 0; i < 256; i++)
    {
       itf.write((char *) &items[i], sizeof(Ui::Item));
    }

    itf.close();

    if(!itf.good())
    {
        ErrorDataFiles(2,true);
    }

    // Weapon Writing to Binary
    if(!wef)
    {
        ErrorDataFiles(3,false);
    }

    for(int i = 0; i < 256; i++)
    {
       wef.write((char *) &weapons[i], sizeof(Ui::Weapon));
    }

    wef.close();

    if(!wef.good())
    {
        ErrorDataFiles(3,true);
    }

    // Armour Writing to Binary
    if(!arf)
    {
        ErrorDataFiles(4,false);
    }

    for(int i = 0; i < 256; i++)
    {
       arf.write((char *) &armours[i], sizeof(Ui::Armour));
    }

    arf.close();

    if(!arf.good())
    {
        ErrorDataFiles(4,true);
    }

    // Enemy Writing to Binary
    if(!enf)
    {
        ErrorDataFiles(5,false);
    }

    for(int i = 0; i < 256; i++)
    {
       enf.write((char *) &enemies[i], sizeof(Ui::Enemy));
    }

    enf.close();

    if(!enf.good())
    {
        ErrorDataFiles(5,true);
    }

    // Classes Writing to Binary
    if(!clf)
    {
        ErrorDataFiles(6,false);
    }

    for(int i = 0; i < 256; i++)
    {
       clf.write((char *) &classy[i], sizeof(Ui::Classy));
    }

    clf.close();

    if(!clf.good())
    {
        ErrorDataFiles(6,true);
    }

    // Last but not the least, writing Text to Text file
    std::ofstream TextData("" + ProjectPath + "Data/TextData.dat");
    std::string line;
    if (TextData.is_open())
    {
        for(int i = 0; i < textData.textDataCount; i++)
        {
          TextData << &textData.data[textData.offsetDatabase[i]];
          TextData << "\n";
        }

        TextData.close();
    }

    this->close();
}

void database::ErrorDataFiles(int filename, bool isWriting)
{
    std::string error[14] = {"Actor","Spells","Items","Weapons","Armours","Enemies","Classys","Troops","States", "Animations", "Tilesets", "Events", "System", "Terms"};

    QMessageBox msgBox;

    if(isWriting)
    {
        msgBox.setText("Binary Data(" + QString::fromStdString(error[filename]) +") is missing or misplaced!\nIf you see this and you are not Drakonchik, please let Drakonchik know this thing showed up!");
    }
    else
    {
        msgBox.setText("Binary Data(" + QString::fromStdString(error[filename]) +") Writing Failed!\nIf you see this and you are not Drakonchik, please let Drakonchik know this thing showed up!");
    }

     msgBox.exec();
}
