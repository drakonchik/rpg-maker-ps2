/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : MAIN                                                     */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : main.cpp                                                 */
/*  User Interface:                                                          */
/*                                                                           */
/*****************************************************************************/

#include "rpgmakerps2.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    RPGMakerPS2 w;
    w.show();
    return a.exec();
}
