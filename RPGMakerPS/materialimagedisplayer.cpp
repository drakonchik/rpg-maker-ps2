#include "materialimagedisplayer.h"
#include "ui_materialimagedisplayer.h"
#include <QPixmap>

materialImageDisplayer::materialImageDisplayer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::materialImageDisplayer)
{
    ui->setupUi(this);
}

materialImageDisplayer::~materialImageDisplayer()
{
    delete ui;
}

void materialImageDisplayer::setUpImage(QString path)
{
    QPixmap picture(path);
    int width = ui->ImageDisplayer->width();
    int height = ui->ImageDisplayer->height();
    ui->ImageDisplayer->setPixmap(picture.scaled(width,height,Qt::KeepAspectRatio));
}
