#ifndef STRINGBLOB_H
#define STRINGBLOB_H

class StringBlob
{
public:
    StringBlob();
    int AddDBString(std::string text);

    char data[4096];
    int length;
    int offsetDatabase[4096];
    int textDataCount = 0;
};

#endif // STRINGBLOB_H
