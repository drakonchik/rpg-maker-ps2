/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : Map Tile and Tileset Tile image handler and data holder  */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : maptile.cpp                                              */
/*  User Interface: rpgmakerps2.ui                                           */
/*                                                                           */
/*****************************************************************************/


/* #### Language Header Files #### */

/* #### QLibrary Header Files #### */
#include <QtWidgets>

/* #### User Header Files     #### */
#include "maptile.h"

/* #### Namespasces and variables #### */


MapTile::MapTile(const QPixmap pixy, int x, int y, int nr, int id, bool isMapTile)
{
    this->x = x;
    this->y = y;
    this->pixy = pixy;
    this->TileNr = nr;
    this->TileID = id;
    this->isSelected = false;
    this->isMapTile = isMapTile;



    //this->setPos(x,y);
    setFlags(ItemIsSelectable);
    //this->setAttribute(Qt::WA_HOVER, true);
    setAcceptHoverEvents(true);
    setCacheMode(QGraphicsItem::NoCache);
}


QRectF MapTile::boundingRect() const
{
    return QRectF(this->x, this->y, 31, 31);
}

QPainterPath MapTile::shape() const
{
    QPainterPath path;
    path.addRect(boundingRect());
    return path;
}

void MapTile::paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget)
{
    Q_UNUSED(widget);

    painter->drawPixmap(x, y, pixy);
    if(isSelected && !isMapTile)
    {
        painter->drawRect(boundingRect());
    }




}

void MapTile::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mousePressEvent(event);

    if(isMapTile)
    {
        // This part will handle the Tile change
        isSelected = true;
        emit clickedOnTilemap();
    }
    else
    {
        isSelected = true;
        emit changedTilesetTile();
    }


}

void MapTile::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}
