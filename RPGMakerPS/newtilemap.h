#ifndef NEWTILEMAP_H
#define NEWTILEMAP_H

#include <QDialog>

namespace Ui {
class newTileMap;
}

class newTileMap : public QDialog
{
    Q_OBJECT

public:
    explicit newTileMap(QWidget *parent = nullptr);
    ~newTileMap();
    void AddTilesetIndexes(QString TileSetName);
    std::string MapName;
    int width;
    int height;
    int tilesetIndex;

private slots:
    void on_ConfirmationRejection_accepted();

private:
    Ui::newTileMap *ui;
};

#endif // NEWTILEMAP_H
