#ifndef OPTIONS_H
#define OPTIONS_H

#include <QDialog>

namespace Ui {
class Options;
}

class Options : public QDialog
{
    Q_OBJECT

public:
    explicit Options(QWidget *parent = nullptr);
    ~Options();
    void setPaths(std::string pcsx,std::string pcsx2,std::string SourcePS1,std::string SourcePS2);
    std::string pcsxPath;
    std::string pcsx2Path;
    std::string SourcePathPS1;
    std::string SourcePathPS2;

private slots:
    void on_pcsxButton_clicked();

    void on_pcsx2Button_clicked();

    void on_RPGSourceButton_clicked();

    void on_ConfirmReject_accepted();

    void on_RPGSourceButtonPS2_clicked();

    void on_RPGSourceButtonPS_clicked();

private:
    Ui::Options *ui;
};

#endif // OPTIONS_H
