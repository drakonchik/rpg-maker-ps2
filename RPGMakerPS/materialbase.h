#ifndef MATERIALBASE_H
#define MATERIALBASE_H

#define ANIMATIONS  "Graphics/Animations/"
#define AUTOTILES   "Graphics/Autotiles/"
#define BACKGROUND  "Graphics/Background/"
#define BATTLERS    "Graphics/Battlers/"
#define CHARACTERS  "Graphics/Characters/"
#define FOGS        "Graphics/Fogs/"
#define FONTS       "Graphics/Fonts/"
#define GUI         "Graphics/GUI/"
#define ICONS       "Graphics/Icons/"
#define IMAGES      "Graphics/Images/"
#define TITLES      "Graphics/Titles/"
#define PORTRAITS   "Graphics/Portraits/"
#define TILESETS    "Graphics/Tilesets/"
#define TRANSITIONS "Graphics/Transitions/"

#include <QDialog>
#include <QString>

namespace Ui {
class materialbase;
}

typedef struct
{
    int Portraits[256];
    int Sprites[2];
}MaterialDataIndexing;


class materialbase : public QDialog
{
    Q_OBJECT

public:
    explicit materialbase(QWidget *parent = nullptr);
    void SetUpFiles(QString PathToProject);
    std::string GetFileNameFromMB(int i, int j);
    std::string GetFilePathFromMB(int i, int j);
    ~materialbase();
    int counts[14];


private slots:
    void on_pushButton_5_clicked();

    void on_DirectoryList_currentRowChanged(int currentRow);

    void on_ButtonPreview_clicked();

private:
    Ui::materialbase *ui;
};

#endif // MATERIALBASE_H
